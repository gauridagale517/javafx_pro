package javafiles.dialoguebox;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
public class checkbox extends Application {
@Override
public void start(Stage stage) {

stage.setTitle("Creating CheckBox");

Label label = new Label("This is a check box demo");
// Array of checkbox labels
String[] checkBoxLabels = {"Core2web", "Incubators", "Biencaps"};

VBox checkHBox = new VBox(20,label);
// Create and add checkboxes to the TilePane
for (String checkBoxLabel : checkBoxLabels) {
CheckBox checkBox = new CheckBox(checkBoxLabel);
// tilePane.getChildren().add(checkBox);

//set the checkbox in VBox
checkHBox.getChildren().add(checkBox);
}

stage.setTitle("RadioButtonDemo");
VBox vbox = new VBox(10); // 10 is the spacing between elements

Label label2 = new Label("Career Option:");

RadioButton radioButton1 = new RadioButton("Backend Developer");
RadioButton radioButton2 = new RadioButton("Web Developer");
RadioButton radioButton3 = new RadioButton("Flutter Developer");
// Create a ToggleGroup
ToggleGroup group = new ToggleGroup();
radioButton1.setToggleGroup(group);
radioButton2.setToggleGroup(group);
radioButton3.setToggleGroup(group);
// Add label and radio buttons to the VBox
vbox.getChildren().addAll(label2, radioButton1, radioButton2,
radioButton3);

Scene scene = new Scene(vbox, 300, 300);
stage.setScene(scene);
stage.show();

}

/* 
Scene scene = new Scene(checkHBox, 200, 200);
stage.setScene(scene);
stage.show();*/
}
