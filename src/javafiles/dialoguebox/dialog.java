package javafiles.dialoguebox;

import java.util.Optional;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.TextField;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
public class dialog extends Application{

    public void start(Stage primaryStage)  {
        TextField text = new TextField();
        Dialog<String>dialog = new Dialog<String>();
        dialog.setTitle("C2w_Dialog");
        ButtonType type = new ButtonType("OK",ButtonData.OK_DONE);
        dialog.setContentText("Hello You Have Created A Dialogue Box");
        dialog.getDialogPane().getButtonTypes().add(type);

        Text txt = new Text("Click here -->");
        Font font = Font.font("verdana",FontWeight.BOLD,FontPosture.REGULAR,12);
        txt.setFont(font);

        Button button = new Button("Show Dialog Box");
        button.setOnAction(e -> extracted(text,dialog));

        HBox pane = new HBox(15);
        pane.setPadding(new Insets(50,150,50,60));
        pane.getChildren().addAll(txt,button);

        Scene sc = new Scene(new Group(pane),600,250,Color.TEAL);
        primaryStage.setTitle("C2w_Dialog Box Demo");
        primaryStage.setScene(sc);
        primaryStage.show();
        
    }

    private void extracted(TextField text, Dialog<String> dialog) {
         dialog.showAndWait(); 
         Alert alert = new Alert(AlertType.INFORMATION);
         alert.setTitle("Information Dialog");
         alert.setHeaderText(null);
         alert.setContentText("I have a great message for you!");
         alert.showAndWait();
         Alert alert1 = new Alert(AlertType.WARNING);
         alert1.setTitle("Warning Dialog");
         alert1.setHeaderText("Look, a Warning Dialog");
         alert1.setContentText("Careful with the next step!");
         alert1.showAndWait();
         Alert alert2 = new Alert(AlertType.ERROR);
         alert2.setTitle("Error Dialog");
         alert2.setHeaderText("Look, an Error Dialog");
         alert2.setContentText("Ooops, there was an error!");
         alert2.showAndWait();
         Alert alert3 = new Alert(AlertType.CONFIRMATION);
         alert3.setTitle("Confirmation Dialog");
         alert3.setHeaderText("Look, a Confirmation Dialog");
         alert3.setContentText("Are you ok with this?");
         Optional<ButtonType> result = alert3.showAndWait();
         if (result.get() == ButtonType.OK){
         // ... user chose OK
         } else {
         // ... user chose CANCEL or closed the dialog

         Alert alert4 = new Alert(AlertType.CONFIRMATION);
alert4.setTitle("Confirmation Dialog with Custom Actions");
alert4.setHeaderText("Look, a Confirmation Dialog with Custom Actions");

alert4.setContentText("Choose your option.");
ButtonType buttonTypeOne = new ButtonType("One");
ButtonType buttonTypeTwo = new ButtonType("Two");
ButtonType buttonTypeThree = new ButtonType("Three");
ButtonType buttonTypeCancel = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);
alert4.getButtonTypes().setAll(buttonTypeOne, buttonTypeTwo, buttonTypeThree,
buttonTypeCancel);
Optional<ButtonType> result1 = alert4.showAndWait();
if (result1.get() == buttonTypeOne){
// ... user chose "One"
} else if (result1.get() == buttonTypeTwo) {
// ... user chose "Two"
} else if (result1.get() == buttonTypeThree) {
// ... user chose "Three"
} else {
// ... user chose CANCEL or closed the dialog
    }
    }}
    
    
}
