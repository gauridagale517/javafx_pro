package javafiles.Home;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class HomeWindow extends Application{
    Text title = null;
    Text message = null;
    Text java = null;
    Text cpp = null;
    Text python = null;
    Text lib = null;
    Text src = null;
    Text bin = null;
    Text main = null;
    Text assets = null;
    Text image = null;
    @Override
    public void start(Stage prStage) {
        prStage.setTitle("myStage");
        prStage.setResizable(false);
        prStage.setWidth(1000);
        prStage.setHeight(800);
        prStage.getIcons().add(new Image("assets/image/logo.jpeg"));

        title = new Text(10,30,"Programming Langauges");
        title.setFont(new Font(40));
        title.setFill(Color.BLUE);

        message = new Text(800,20,"Good Evening");
        message.setFont(new Font(30));
        message.setFill(Color.ORANGE);

        bin = new Text("bin");
        bin.setFont(new Font(25));
        bin.setFill(Color.MAGENTA);

        src = new Text("src");
        src.setFont(new Font(25));
        src.setFill(Color.GRAY);

        lib = new Text("lib");
        lib.setFont(new Font(25));
        lib.setFill(Color.BROWN);

        VBox vb2 = new VBox(20,bin,lib,src);
        vb2.setAlignment(Pos.CENTER);
        vb2.setLayoutX(100);
        vb2.setLayoutY(300);

        java = new Text("java");
        java.setFont(new Font(25));
        java.setFill(Color.RED);

        python = new Text("python");
        python.setFont(new Font(25));
        python.setFill(Color.GREEN);

        cpp = new Text("cpp");
        cpp.setFont(new Font(25));
        cpp.setFill(Color.RED);

        

        VBox vb1 = new VBox(20,java,python,cpp);
        //vb2.setAlignment(Pos.CENTER);
        vb1.setLayoutX(100);
        vb1.setLayoutY(300);

        HBox hb = new HBox(100,vb1,vb2);
        hb.setLayoutX(100);
        hb.setLayoutY(200);

        Group gr = new Group(hb,title,message);

        Scene sc = new Scene(gr);
        sc.setFill(Color.AQUA);
        prStage.setScene(sc);

        prStage.show();
    }
    
}
