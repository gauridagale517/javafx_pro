package javafiles.Home;

import javafx.application.Application;
//import javafx.scene.Group;
//import javafx.geometry.Pos;
//import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
//import javafx.scene.layout.Pane;
//import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
//import javafx.scene.text.Text;
import javafx.stage.Stage;

public class MyPane extends Application {
    Label lb = null;
    Label topl = null;
    Label bottoml = null;
    Label topr = null;
    Label bottomr = null;

    @Override
    public void start(Stage primaryStage) {

        lb = new Label("Good Evening");
        //lb.setAlignment(Pos.BASELINE_CENTER);
        lb.setFont(Font.font(30));
        lb.setTextFill(Color.BLACK);

        topl = new Label("TOP");
        topl.setFont(Font.font(20));
        topl.setTextFill(Color.BLACK);

        bottoml = new Label("BOTTOM");
        bottoml.setFont(Font.font(20));
        bottoml.setTextFill(Color.BLUE);

        topr = new Label("TOP RIGHT");
        topr.setFont(Font.font(20));
        topr.setTextFill(Color.BLACK);
        
        bottomr = new Label("BOTTOM RIGHT");
        bottomr.setFont(Font.font(20));
        bottomr.setTextFill(Color.BLACK);
        

        

        BorderPane bp = new BorderPane();
        bp.setCenter(lb);
        bp.setBottom(bottoml);
        bp.setTop(topl);
        bp.setRight(topr);
        //bp.setLeft(bottomr);
        //bp.setOpacity(1);


       /*  StackPane st = new StackPane();
        st.getChildren().addAll(lb);
        st.setAlignment(Pos.TOP_CENTER);*/
        Scene sc = new Scene(bp,1000,800);
        sc.setFill(Color.AQUA);
        primaryStage.setScene(sc);
        primaryStage.show();
        
    }
    
}
