package javafiles.myButton;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class myButton extends Application {

    public void start(Stage prStage) {
        Label username = new Label("Enter Username");
        username.setFont(Font.font(20));
        username.setTextFill(Color.BROWN);
        Label password = new Label("Enter password");
        password.setFont(Font.font(20));
        password.setTextFill(Color.BROWN);
        TextField tx = new TextField();
        tx.setAlignment(Pos.CENTER);
        tx.setMaxSize(400,10);
        PasswordField ps = new PasswordField();
        ps.setAlignment(Pos.CENTER);
        ps.setMaxSize(400,10);
        
        Button show = new Button("Show");
        show.setOnAction(new EventHandler<ActionEvent>() {

            public void handle(ActionEvent event) {
                
                System.out.println(tx.getText());
                System.out.println(ps.getText());
                
            }         
        });
   VBox vb = new VBox(20,username,tx,password,ps,show);
   vb.setAlignment(Pos.CENTER);
   vb.setStyle("-fx-background-color:PINK");
   Scene sc = new Scene(vb,1000,1000);
   sc.setFill(Color.GREEN);
   prStage.setScene(sc);
   prStage.show();        
   }
    
}
