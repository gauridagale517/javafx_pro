package javafiles.imagePage;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class imagePage extends Application {
    
    @Override
    public void start(Stage primaryStage) {

    Image ig = new Image("assets/image/logo.jpeg");
    ImageView iv = new ImageView(ig);
    iv.setFitWidth(300);
    iv.setFitHeight(200);
    iv.setPreserveRatio(true);
    

    Label imglb = new Label("myImage");
    imglb.setFont(Font.font(30));
    imglb.setTextFill(Color.CHOCOLATE);
    imglb.setAlignment(Pos.CENTER);

    
    HBox hb = new HBox(15,iv,imglb);
    hb.setLayoutX(10);
    hb.setLayoutY(20);
    hb.setAlignment(Pos.CENTER);
    hb.setStyle("-fx-background-color:PINK");

    Group gr = new Group(hb);
    Scene sc = new Scene(gr,1800,1000,Color.AQUA);

    primaryStage.setScene(sc);
    primaryStage.show();
        
    }
}
